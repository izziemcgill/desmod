#!/usr/bin/nodejs
console.log('\x1b[2J\x1b[2;2H\x1b[1m\x1b[37m%s\x1b[0m', ' Hello, World!');

const http = require('http');
const qs = require('querystring');
const fs = require('fs');
const path = require('path');
const url = require('url')

const Sequelize = require('sequelize');

//const sequelize = new Sequelize('test.db', '', '', {
//  storage: 'test.db',
//  dialect: 'sqlite'
//});

//MSSQL
const sequelize = new Sequelize('PRODAPPDB', 'lombardi', 'lombardi', {
  host: 'DEUSDHEID0033',
  dialect: 'mssql'
});

const User = sequelize.define('user', {
    name: Sequelize.STRING
  , user: Sequelize.STRING
  , dept: Sequelize.STRING
  , role: Sequelize.STRING
  , boss: Sequelize.STRING
  , email: Sequelize.STRING
}, {
   tableName: 'users'
 , timestamps: false
});
const Op = Sequelize.Op

http.createServer((request, response) => {

  console.log('\x1b[1m\x1b[34m Request \x1b[0m %s ', request.method);
  console.log(request.headers);

  // crude but simple routing
  if (request.method === 'GET') {

    // get data
    parts = url.parse(request.url).path.split('/')
    if (parts[1] === 'api') {

      request.params = qs.parse(request.url.split('?')[1]);
      if (params.hasOwnProperty('q')) {
        query = '%' + request.params.q + '%';
        var data = []
        User.findAll({
          where: {
            [Op.or]: [
              {
                id: {
                  [Op.like]: query
                }
              },
              {
                user: {
                  [Op.like]: query
                }
              },
              {
                name: {
                  [Op.like]: query
                }
              },
              {
                role: {
                  [Op.like]: query
                }
              },
              {
                dept: {
                  [Op.like]: query
                }
              }
            ]
          }
        }).then(users => {
          users.forEach((user)=>{ data.push(user.dataValues) })
          return data;
        }).then(data => {
          var contentType = 'application/json';
          var content = JSON.stringify(data);
          response.writeHead(200, { 'Content-Type': contentType });
          response.end(content, 'utf-8');
        });
      } else {
        //@todo Fix this - router stuff
        //@body Separate the data api from the file server, 
        console.error('User id missing');
        

      }
    } else {
        // get file
        var filePath = '.' + request.url;
        if (filePath == './') {
          filePath = './index.html';
        }

        // shorthand paths
        var extname = path.extname(filePath);
        if (extname === "") {
          console.log('\x1b[1m\x1b[34m Path \x1b[0m %s ', filePath);
          filePath += '.html'
        }

        // Set allowed content types
        var contentType = 'text/html';
        switch (extname) {
          case '.css':
            contentType = 'text/css';
            break;
          case '.gif':
            contentType = 'image/gif';
            break;
          case '.json':
            contentType = 'application/json';
            break;
        }
        console.log('\x1b[1m\x1b[34m Content type \x1b[0m %s ', contentType);

        fs.readFile(filePath, function(error, content) {
          if (error) {
            if(error.code == 'ENOENT'){
              console.error('\x1b[1m\x1b[31m Resource not found! filepath was:\x1b[0m %s', filePath);
              fs.readFile('./pages/404.html', function(error, content) {
                response.writeHead(404, { 'Content-Type': contentType });
                response.end(content, 'utf-8');
              });
            }
            else {
              console.error('\x1b[1m\x1b[31m Unexpected error exception! filepath was: \x1b[0m %s', filePath);
              fs.readFile('./pages/500.html', function(error, content) {
                response.writeHead(500, { 'Content-Type': contentType });
                response.end(content, 'utf-8');
              });
            }
          }
          else {
            console.log('\x1b[1m\x1b[32m Response \x1b[0m %s ', filePath);
            response.writeHead(200, { 'Content-Type': contentType });
            response.end(content, 'utf-8');
          }
        });
    }
  } else if (request.method === "POST") {

    if (request.url === "/form") {

      var requestBody = '';
      request.on('data', function(data) {
        requestBody += data;
        if(requestBody.length > 1e7) {
          console.error('Request Entity Too Large: ');
          fs.readFile('./pages/413.html', function(error, content) {
            response.writeHead(413, { 'Content-Type': contentType });
            response.end(content, 'utf-8');
          });
        }
      });
      request.on('end', function() {
        var formData = qs.parse(requestBody);
        response.writeHead(200, {'Content-Type': 'text/html'});
        response.write('<!doctype html><html><head><title>response</title></head><body>');
        response.write('Thanks for the data!<br />User Name: '+formData.UserName);
        response.write('<br />Repository Name: '+formData.Repository);
        response.write('<br />Branch: '+formData.Branch);
        response.end('</body></html>');
      });

    } else {
      console.error('\x1b[1m\x1b[31m Resource not found! filepath was:\x1b[0m %s', filePath);
      fs.readFile('./pages/404.html', function(error, content) {
        response.writeHead(404, { 'Content-Type': contentType });
        response.end(content, 'utf-8');
      });
    }

  } else {
    console.error('\x1b[1m\x1b[31m Method not supported! method was:\x1b[0m %s', request.method);
    fs.readFile('./pages/405.html', function(error, content) {
      response.writeHead(405, { 'Content-Type': contentType });
      response.end(content, 'utf-8');
    });
  }

}).listen(80)

console.log('\x1b[33m%s\x1b[0m', '  *** One server thread listening on localhost:80');
